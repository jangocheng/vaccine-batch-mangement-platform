﻿using BootstrapBlazor.Components;
using BootstrapBlazorApp.Shared.Client;
using BootstrapBlazorApp.Shared.Data;


using System.Text;

var builder = WebApplication.CreateBuilder(args);
var pomainBaseApiUrl = builder.Configuration["MisApiAddress:PomainBaseApiUrl"];
// Add services to the container.
Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();

builder.Services.AddBootstrapBlazor();
builder.Services.AddSingleton<WeatherForecastService>();


builder.Services.AddHttpClient<VaccinationPointClient>(
              client => client.BaseAddress = new Uri(pomainBaseApiUrl));

builder.Services.AddHttpClient<PODemandClient>(
              client => client.BaseAddress = new Uri(pomainBaseApiUrl));

builder.Services.AddHttpClient<VaccineClient>(
              client => client.BaseAddress = new Uri(pomainBaseApiUrl));




builder.Services.AddSingleton<VaccineService>();
builder.Services.AddSingleton<CompanyService>();
builder.Services.AddSingleton<GatewayService>();
builder.Services.AddSingleton<VaccineBatchRecordService>();
builder.Services.AddSingleton<VaccineDeliveryRecordService>();

builder.Services.AddCors(policy =>
{
    policy.AddPolicy("CorsPolicy", opt => opt
        .AllowAnyOrigin()
        .AllowAnyHeader()
        .AllowAnyMethod());
});
builder.Services.AddOidcAuthentication(options =>
{
    builder.Configuration.Bind("OidcAuthentication", options.ProviderOptions);
    options.ProviderOptions.Authority = "https://localhost:7024";
    options.AuthenticationPaths.LogOutSucceededPath = "";
});
// 增加 Table 数据服务操作类
builder.Services.AddTableDemoDataService();
//请求方式一
builder.Services.AddScoped(sp => new HttpClient
{
    BaseAddress = new Uri("http://localhost:5140/")
});
//请求方式二
builder.Services.AddHttpClient("api", c =>
{
    c.BaseAddress = new Uri("http://localhost:5140/api/v1/");
    c.DefaultRequestHeaders.Add("Accept", "application/vnd.github.v3+json");
});


var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Error");
}

app.UseStaticFiles();
app.UseRouting();
app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();
