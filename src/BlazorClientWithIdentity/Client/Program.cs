using BlazorApp1.Client;

using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
builder.Services.AddOidcAuthentication(options =>
{
    builder.Configuration.Bind("OidcAuthentication", options.ProviderOptions);
    options.ProviderOptions.Authority = "https://localhost:7204";
    options.AuthenticationPaths.LogOutSucceededPath = "";
});
await builder.Build().RunAsync();
