﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace API.Entities
{
    #region 【疫苗需求明细】
    /// <summary>
    /// 疫苗需求明细
    /// </summary>
    [Table("PC_PODetails")]
    public class PODetails
    {

        #region 【构造器】
        private PODetails()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="vaccineId"></param>
        /// <param name="quantity"></param>
        public PODetails(Guid id, Guid? poDemandId, Guid? vaccineId, decimal quantity) 
        {
            Id = id;
            POMainId = poDemandId;
            SetVaccineId(vaccineId);
            SetQuantity(quantity);
            TotalReceiveQty = 0;
        }

        #endregion

        #region 【属性】

        /// <summary>
        /// KEY
        /// </summary>
        public virtual Guid Id { get;  set; }

        /// <summary>
        /// 需求主表ID
        /// </summary>
        public virtual Guid? POMainId { get; set; }

        /// <summary>
        /// 疫苗ID
        /// </summary>
        public virtual Guid? VaccineId { get;  set; }
        /// <summary>
        /// 数量
        /// </summary>
        public virtual decimal Quantity { get;  set; }

        /// <summary>
        /// 累计收货数量
        /// </summary>
        public virtual decimal TotalReceiveQty { get; set; }


        #endregion

        #region 【方法】

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vaccineId"></param>
        private void SetVaccineId(Guid? vaccineId)
        {
            VaccineId = vaccineId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quantity"></param>
        private void SetQuantity(decimal quantity)
        {
            Quantity = quantity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="totalReceiveQty"></param>
        internal void ChangeTotalReceiveQty(decimal totalReceiveQty)
        {
            TotalReceiveQty = totalReceiveQty;
        }

        #endregion
    }
    #endregion
}
