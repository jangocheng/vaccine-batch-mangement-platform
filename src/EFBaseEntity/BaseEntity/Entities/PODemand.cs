﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace API.Entities
{
    #region 【采购需求主表】
    /// <summary>
    /// 采购需求主表
    /// </summary>
    [Table("PC_PODemand")]
    public class PODemand
    {

        #region 【构造器】
        private PODemand() { }

        public PODemand(Guid id, string code,Guid? vaccinationPointId, Guid? manufactorId, DateTime demandDate)
        {
            Id=id;
            SetCode(code);
            SetManufactorId(manufactorId);
            SetDemandDate(demandDate);
            VaccinationPointId = vaccinationPointId;
            Items = new List<PODetails>();
        }
        #endregion

        #region 【属性】

        public virtual Guid Id { get; set; }

        /// <summary>
        /// 需求单号
        /// </summary>
        public virtual string Code { get;  set; }

        /// <summary>
        /// 接种点ID
        /// </summary>
        public virtual Guid? VaccinationPointId { get; set; }


        /// <summary>
        /// 生产厂家ID
        /// </summary>
        public virtual Guid? ManufactorId { get;  set; }


        /// <summary>
        /// 需求日期
        /// </summary>
        public virtual DateTime? DemandDate { get;  set; }

        /// <summary>
        /// 疫苗需求明细
        /// </summary>
        public ICollection<PODetails> Items { get; set; }

        #endregion

        #region 【方法】

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        internal void SetCode(string code) {
            Code = code;
        }

        /// <summary>
        /// 生产厂家ID
        /// </summary>
        /// <param name="ManufactorId"></param>
        internal void SetManufactorId(Guid? manufactorId)
        {
            ManufactorId = manufactorId;
        }

        /// <summary>
        /// 需求日期
        /// </summary>
        /// <param name="demandDate"></param>
        /// <returns></returns>
        private void SetDemandDate(DateTime? demandDate)
        {
            DemandDate = demandDate;
        }



        public virtual PODetails FindPODetails(Guid PODetailsId)
        {
          
            return Items.FirstOrDefault(c => c.Id == PODetailsId);
        }

        public virtual void AddPODetails(PODetails PODetailss)
        {
            if (!IsInItem(PODetailss.Id))
            {
                Items.Add(PODetailss);
            }
        }


        public virtual void AddPODetailss(IEnumerable<PODetails> PODetailsss)
        {
            foreach (var item in PODetailsss)
            {
                AddPODetails(item);
            }
        }

        public virtual void RemovePODetails(PODetails PODetailss)
        {
            if (IsInItem(PODetailss.Id))
            {
                Items.Remove(PODetailss);
            }
        }

        public virtual void RemovePODetails(Guid PODetailssId)
        {
            var item = FindPODetails(PODetailssId);
            if (item != null)
            {
                RemovePODetails(item);
            }
        }


        public virtual bool IsInItem(Guid PODetailssId)
        {
            return Items.Any(r => r.Id == PODetailssId);
        }


        #endregion


    }
    #endregion
    
}
