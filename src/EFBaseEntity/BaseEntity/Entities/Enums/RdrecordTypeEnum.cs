﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Enums
{
    public enum RdrecordTypeEnum
    {
        /// <summary>
        /// 出库
        /// </summary>
        OUT = 0,
        /// <summary>
        /// 入库
        /// </summary>
        IN =1,

    }
}
