﻿using System.ComponentModel.DataAnnotations.Schema;

namespace API.Entities
{
    /// <summary>
    ///  接种点基础信息
    /// </summary>
    [Table("AA_VaccinationPoint")]
    public class VaccinationPoint
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        public string Principal { get; set; }
    }
}
