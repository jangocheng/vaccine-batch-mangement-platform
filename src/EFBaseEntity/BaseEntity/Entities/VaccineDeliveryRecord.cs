﻿using System.ComponentModel.DataAnnotations.Schema;

namespace API.Entities
{
    /// <summary>
    /// 疫苗发货记录
    /// </summary>
    [Table("PC_VaccineDeliveryRecord")]
    public class VaccineDeliveryRecord
    {
        public Guid Id { get; set; }

        public Guid PurchaseId { get; set; }

        public Guid VaccinationPointId { get; set; }
        public Guid VaccineBatchRecordId { get; set; }
        public DateTime DeliveryDate { get; set; }

        public int Qty { get; set; }
    }
}
