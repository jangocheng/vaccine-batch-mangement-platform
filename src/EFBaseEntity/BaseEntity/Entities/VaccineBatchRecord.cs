﻿using System.ComponentModel.DataAnnotations.Schema;

namespace API.Entities
{
    /// <summary>
    /// 疫苗批次档案记录
    /// </summary>
    [Table("PC_VaccineBatchRecord")]
    public class VaccineBatchRecord
    {
        public Guid Id { get; set; }
        public int Type { get; set; }
        public string BatchCode { get; set; }
        public Guid VaccineId { get; set; }
        
        public DateTime ProductionDate { get; set; }

        public DateTime ValidDate { get; set; }
        public int Qty { get; set; }
    }
}
