﻿using API.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Entities
{
    #region 【收发记录】
    /// <summary>
    /// 收发记录
    /// </summary>
    [Table("ST_Rdrecord")]
    public class Rdrecord
    {
        #region 【构造器】
        private Rdrecord() { }


        public Rdrecord(Guid id, RdrecordTypeEnum rdrecordType,Guid vaccinationPointId, Guid manufactorId, string code,DateTime voucherDate) {
            Id = id;
            RdrecordType =rdrecordType;
            SetVaccinationPointId(vaccinationPointId);
            SetManufactorId(manufactorId);
            SetCode(code);
            SetVoucherDate(voucherDate);
            Items = new List<RdrecordDetails>();
        }
        #endregion

        #region 【属性】

        /// <summary>
        /// 
        /// </summary>
        public virtual Guid Id { get; set; }
        /// <summary>
        /// 收发类型
        /// </summary>
        public virtual RdrecordTypeEnum RdrecordType { get; private set; }

        /// <summary>
        /// 接种站点ID
        /// </summary>
        public virtual Guid VaccinationPointId { get; private set; }

        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get; private set; }

        /// <summary>
        /// 单据日期
        /// </summary>
        public virtual DateTime VoucherDate { get; private set; }

        /// <summary>
        /// 生产厂家ID
        /// </summary>
        public virtual Guid ManufactorId { get; private set; }

    
        /// <summary>
        /// 入库疫苗明细
        /// </summary>
        public ICollection<RdrecordDetails> Items { get; private set; }

        #endregion
        #region 【方法】

        /// <summary>
        /// 设置收发类型
        /// </summary>
        /// <param name="rdrecordType"></param>
        internal void SetRdrecordType(RdrecordTypeEnum rdrecordType)
        {
            RdrecordType = rdrecordType;
        }

        /// <summary>
        /// 生产厂家ID
        /// </summary>
        /// <param name="ManufactorId"></param>
        internal void SetManufactorId(Guid manufactorId)
        {
            ManufactorId = manufactorId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        internal void SetCode(string code)
        {
            Code = code;
        }

        /// <summary>
        /// 接种站点ID
        /// </summary>
        /// <param name="vaccinationPointId"></param>
        internal void SetVaccinationPointId(Guid vaccinationPointId)
        {
            VaccinationPointId = vaccinationPointId;
        }

        /// <summary>
        /// 单据日期
        /// </summary>
        /// <param name="voucherDate"></param>
        /// <returns></returns>
        private void SetVoucherDate(DateTime voucherDate)
        {
            VoucherDate = voucherDate;
        }

      
        /// <summary>
        /// 查找入库明细
        /// </summary>
        /// <param name="RdrecordDetailsId"></param>
        /// <returns></returns>
        public virtual RdrecordDetails FindRdrecordDetails(Guid RdrecordDetailsId)
        {

            return Items.FirstOrDefault(c => c.Id == RdrecordDetailsId);
        }
        /// <summary>
        /// 添加入库明细
        /// </summary>
        /// <param name="RdrecordDetailss"></param>
        public virtual void AddRdrecordDetails(RdrecordDetails RdrecordDetailss)
        {
            if (!IsInItem(RdrecordDetailss.Id))
            {
                Items.Add(RdrecordDetailss);
            }
        }

        /// <summary>
        /// 添加入库明细
        /// </summary>
        /// <param name="RdrecordDetailsss"></param>
        public virtual void AddRdrecordDetailss(IEnumerable<RdrecordDetails> RdrecordDetailsss)
        {
            foreach (var item in RdrecordDetailsss)
            {
                AddRdrecordDetails(item);
            }
        }
        /// <summary>
        /// 删除明细项
        /// </summary>
        /// <param name="RdrecordDetailss"></param>
        public virtual void RemoveRdrecordDetails(RdrecordDetails RdrecordDetailss)
        {
            if (IsInItem(RdrecordDetailss.Id))
            {
                Items.Remove(RdrecordDetailss);
            }
        }
        /// <summary>
        /// 删除明细项
        /// </summary>
        /// <param name="RdrecordDetailssId"></param>
        public virtual void RemoveRdrecordDetails(Guid RdrecordDetailssId)
        {
            var item = FindRdrecordDetails(RdrecordDetailssId);
            if (item != null)
            {
                RemoveRdrecordDetails(item);
            }
        }

        /// <summary>
        /// 是否存在明细项
        /// </summary>
        /// <param name="RdrecordDetailssId"></param>
        /// <returns></returns>
        public virtual bool IsInItem(Guid RdrecordDetailssId)
        {
            return Items.Any(r => r.Id == RdrecordDetailssId);
        }


        #endregion

    }
    #endregion
    
}
