﻿using System.ComponentModel.DataAnnotations.Schema;

namespace API.Entities
{
    [Table("AA_Vaccine")]
    public class Vaccine
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public string Specifications { get; set; }
        public Guid CompanyId { get; set; }

        public string Principal { get; set; }
    }
}
