﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace API.Entities
{
    #region 【收发记录明细】
    /// <summary>
    /// 收发记录明细
    /// </summary>
    [Table("ST_RdrecordDetails")]
    public class RdrecordDetails
    {

        #region 【构造器】
        private RdrecordDetails()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="vaccineId"></param>
        /// <param name="quantity"></param>
        /// <param name="poDetailsId"></param>
        /// <param name="batchCode"></param>
        /// <param name="sn"></param>
        public RdrecordDetails(Guid id,Guid rdrecordId ,Guid vaccineId, decimal quantity, Guid? poDetailsId = null, string batchCode = "", string sn = "", DateTime? mnDate = null, DateTime? validityDate = null)
        {
            Id = id;
            SetRdrecordId(rdrecordId);
            SetVaccineId(vaccineId);
            SetQuantity(quantity);
            SetBatchCode(batchCode);
            SetPODetailsId(poDetailsId);
            SetSN(sn);
            SetMNDate(mnDate);
            SetValidityDate(validityDate);
        }

        #endregion

        #region 【属性】

        /// <summary>
        /// 明细ID
        /// </summary>
        public virtual Guid Id { get;  set; }

        /// <summary>
        /// 主表ID
        /// </summary>
        public virtual Guid RdrecordId { get;  set; }

        /// <summary>
        /// 采购需求明细ID
        /// </summary>
        public virtual Guid? PODetailsId { get;  set; }

        /// <summary>
        /// 疫苗ID
        /// </summary>
        public virtual Guid VaccineId { get;  set; }

        /// <summary>
        /// 收货数量
        /// </summary>
        public virtual decimal Quantity { get;  set; }


        /// <summary>
        /// 批次号
        /// </summary>
        public virtual string BatchCode { get;  set; }

        /// <summary>
        /// 疫苗SN
        /// </summary>
        public virtual string SN { get;  set; }

        /// <summary>
        /// 生产日期
        /// </summary>
        public virtual DateTime? MNDate { get;  set; }

        /// <summary>
        /// 有效期
        /// </summary>
        public virtual DateTime? ValidityDate { get;  set; }

        #endregion

        #region 【方法】

        private void SetRdrecordId(Guid rdrecordId)
        {
            RdrecordId = rdrecordId;
        }


        private void SetPODetailsId(Guid? poDetailsId)
        {
            PODetailsId = poDetailsId;
        }

        /// <summary>
        /// 设置疫苗ID
        /// </summary>
        /// <param name="vaccineId"></param>
        private void SetVaccineId(Guid vaccineId)
        {
            VaccineId = vaccineId;
        }

        /// <summary>
        /// 设置需求数量
        /// </summary>
        /// <param name="quantity"></param>
        private void SetQuantity(decimal quantity)
        {
            Quantity = quantity;
        }

        /// <summary>
        /// 设置批次号
        /// </summary>
        /// <param name="batchCode"></param>
        private void SetBatchCode(string batchCode)
        {
            BatchCode = batchCode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sn"></param>
        /// <returns></returns>
        private void SetSN(string sn)
        {
            SN = sn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mnDate"></param>
        private void SetMNDate(DateTime? mnDate)
        {
            MNDate = mnDate;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="validityDate"></param>
        private void SetValidityDate(DateTime? validityDate)
        {
            ValidityDate = validityDate;
        }

        #endregion
    }
    #endregion
}
