﻿namespace DaprWebApiDemo.Entitys
{
    public class VaccineBatchRecord
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string BatchCode { get; set; }
        public Guid VaccineId { get; set; }
        public DateTime ProductionDate { get; set; }

        public DateTime ValidDate { get; set; }
        public int Qty { get; set; }
    }
}
