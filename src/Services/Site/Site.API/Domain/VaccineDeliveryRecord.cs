﻿namespace DaprWebApiDemo.Entitys
{
    public class VaccineDeliveryRecord
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string BatchCode { get; set; }
        public Guid VaccineBatchRecordId { get; set; }
        public DateTime DeliveryDate { get; set; }

        public int Qty { get; set; }
    }
}
