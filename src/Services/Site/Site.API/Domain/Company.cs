﻿namespace DaprWebApiDemo.Entitys
{
    /// <summary>
    /// 厂家
    /// </summary>
    public class Company
    {
        public Guid Id { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
