﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POMain.API.Dtos;
using POMain.API.Infrastructure;
using POMain.API.ViewModel;
using System.Net;

namespace POMain.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class RdrecordController : ControllerBase
    {
        private readonly PODbContext _context;

        public RdrecordController(PODbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));

            _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        [HttpGet("paged/recordfo")]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<RdrecordInfoDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<PaginatedItemsViewModel<RdrecordInfoDto>>> ItemsAsync(
            string? keyword = "",
            [FromQuery] int pageSize = 10,
            [FromQuery] int pageIndex = 0)
        {

            var query = from rdDetail in _context.RdrecordDetailss
                        join rdMain in _context.Rdrecords on rdDetail.RdrecordId equals rdMain.Id into temp
                        from t1 in temp.DefaultIfEmpty()
                        join vaccines in _context.Vaccines on rdDetail.VaccineId equals vaccines.Id into temp2
                        from t2 in temp2.DefaultIfEmpty()
                        join company in _context.Companies on t1.ManufactorId equals company.Id into temp3
                        from t3 in temp3.DefaultIfEmpty()
                        join vaccinationPoints in _context.VaccinationPoints on t1.VaccinationPointId equals vaccinationPoints.Id into temp4
                        from t4 in temp4.DefaultIfEmpty()
                        select new RdrecordInfoDto
                        {
                            Id = rdDetail.Id,
                            RdrecordId = rdDetail.RdrecordId,
                            Quantity = rdDetail.Quantity,
                            CompanyName = t3.CompanyName,
                            VaccinationPointName = t4.Name,
                            VaccineCode = t2.Code,
                            VaccineName = t2.Name,
                            VaccineId = rdDetail.VaccineId,
                            BatchCode = rdDetail.BatchCode,
                            Code = t1.Code,
                            ManufactorId = t1.ManufactorId,
                            MNDate = rdDetail.MNDate,
                            PODetailsId = rdDetail.PODetailsId,
                            RdrecordType  = t1.RdrecordType,
                            SN = rdDetail.SN,
                            VaccinationPointId = t1.VaccinationPointId,
                            ValidityDate = rdDetail.ValidityDate,
                            VoucherDate =t1.VoucherDate,
                        };
            if (!string.IsNullOrWhiteSpace(keyword))
            {
                query = query.Where(x => x.Code.Contains(keyword) || x.CompanyName.Contains(keyword) || x.VaccinationPointName.Contains(keyword) || x.VaccineCode.Contains(keyword) || x.VaccineName.Contains(keyword));
            }


            var totalItems = await query
                .LongCountAsync();

            var itemsOnPage = await query
                .OrderByDescending(item => item.Code)
                .Skip(pageSize * pageIndex)
                .Take(pageSize)
                .ToListAsync();

            return new PaginatedItemsViewModel<RdrecordInfoDto>(pageIndex, pageSize, totalItems, itemsOnPage);
        }



    }
}
