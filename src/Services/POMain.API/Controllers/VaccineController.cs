﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POMain.API.Dtos;
using API.Entities;
using POMain.API.Infrastructure;
using System.Linq;
using System.Net;

namespace POMain.API.Controllers
{

    [Route("api/v1/[controller]")]
    [ApiController]
    public class VaccineController : ControllerBase
    {

        private readonly PODbContext _context;

        public VaccineController(PODbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));

            _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        /// <summary>
        /// 疫苗清单
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        [ProducesResponseType(typeof(List<VaccineDto>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<VaccineDto>>> GetVaccinesAsync(Guid? companyId)
        {
            var query = _context.Vaccines
                    .Select(x => new VaccineDto {
                        Id = x.Id,
                        Name = x.Name,
                        Principal = x.Principal,
                        Code = x.Code,
                        CompanyId = x.CompanyId,
                        Specifications = x.Specifications,
                    });
            if (companyId.HasValue) {
                query = query.Where(x => x.CompanyId == companyId);

            }
            var dtos  = await  query.ToListAsync();

            return dtos;
        }

        /// <summary>
        /// 疫苗清单
        /// </summary>
        /// <returns></returns>
        [HttpGet("all2")]
        public List<VaccineDto> GetAll(Guid? companyId)
        {
            var query = _context.Vaccines
                    .Select(x => new VaccineDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Principal = x.Principal,
                        Code = x.Code,
                        CompanyId = x.CompanyId,
                        Specifications = x.Specifications,
                    });
            if (companyId.HasValue)
            {
                query = query.Where(x => x.CompanyId == companyId);

            }
            var dtos = query.ToList();

            return dtos;
        }



        /// <summary>
        /// 创建疫苗档案
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task CreateAsync(VaccineEditDto input)
        {
            if (!_context.Vaccines.Any(x => x.Code == input.Code))
            {
                var entity = new Vaccine
                {
                    Id = Guid.NewGuid(),
                    Code = input.Code,
                    Name = input.Name,
                    CompanyId = input.CompanyId,
                    Principal = input.Principal,
                    Specifications = input.Specifications,
                };

                _context.Add(entity);
                await _context.SaveChangesAsync();
            }
            else {
                var entity = await _context.Vaccines.Where(x => x.Code == input.Code).FirstOrDefaultAsync();
                entity.Name = input.Name;
                entity.Specifications = input.Specifications;
                entity.CompanyId = input.CompanyId;
                entity.Principal = input.Principal;

                _context.Update(entity);
                await _context.SaveChangesAsync();

            }

          


        }

        /// <summary>
        /// 更新疫苗档案
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("update")]
        public async Task UpdateAsync(Guid id,VaccineEditDto input)
        {
            var entity = await _context.Vaccines.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (entity == null)
            {
                entity = new Vaccine
                {
                    Id = Guid.NewGuid(),
                    Code = input.Code,
                    Name = input.Name,
                    CompanyId = input.CompanyId,
                    Principal = input.Principal,
                    Specifications = input.Specifications,
                };

                 _context.Add(entity);
                    await _context.SaveChangesAsync();
            }
            else {
                entity.Name = input.Name;
                entity.Code = input.Code;
                entity.Specifications = input.Specifications;
                entity.CompanyId = input.CompanyId;
                entity.Principal = input.Principal;

                _context.Update(entity);
                await _context.SaveChangesAsync();
            }

           
        }

        /// <summary>
        /// 删除疫苗档案
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete")]
        public async Task UpdateAsync(Guid id)
        {
            var entity = await _context.Vaccines.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (entity != null) {
                _context.Remove<Vaccine>(entity);
                await _context.SaveChangesAsync();
            }
        }
    }
}
