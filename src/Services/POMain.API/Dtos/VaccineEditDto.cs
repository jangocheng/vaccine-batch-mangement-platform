﻿namespace POMain.API.Dtos
{
    public class VaccineEditDto
    {
       

        public string Name { get; set; }
        public string Code { get; set; }

        public string Specifications { get; set; }
        public Guid CompanyId { get; set; }

        public string Principal { get; set; }
    }
}
