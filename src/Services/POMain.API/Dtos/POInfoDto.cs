﻿namespace POMain.API.Dtos
{
    public class POInfoDto
    {

        /// <summary>
        /// 需求ID
        /// </summary>
        public virtual Guid? Id { get; set; }

        /// <summary>
        /// 需求主表ID
        /// </summary>
        public virtual Guid? POMainId { get; set; }

        /// <summary>
        /// 疫苗ID
        /// </summary>
        public virtual Guid? VaccineId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public virtual decimal Quantity { get; set; }

        /// <summary>
        /// 累计收货数量
        /// </summary>
        public virtual decimal TotalReceiveQty { get; set; }


        /// <summary>
        /// 需求单号
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 生产厂家ID
        /// </summary>
        public virtual Guid? ManufactorId { get; set; }


        /// <summary>
        /// 需求日期
        /// </summary>
        public virtual DateTime? DemandDate { get; set; }

        /// <summary>
        /// 疫苗名称
        /// </summary>
        public virtual string VaccineName {get;set;}
        /// <summary>
        /// 疫苗编号
        /// </summary>
        public virtual string VaccineCode { get; set; }


        /// <summary>
        /// 生产厂家
        /// </summary>
        public virtual string CompanyName { get; set; }

        /// <summary>
        /// 接种站点ID
        /// </summary>
        public virtual Guid? VaccinationPointId { get; set; }

        /// <summary>
        /// 接种点名称
        /// </summary>
        public virtual string VaccinationPointName { get; set; }

    }
}
