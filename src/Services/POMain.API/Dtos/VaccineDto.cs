﻿namespace POMain.API.Dtos
{
    public class VaccineDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public string Specifications { get; set; }
        public Guid CompanyId { get; set; }

        public string Principal { get; set; }
    }
}
