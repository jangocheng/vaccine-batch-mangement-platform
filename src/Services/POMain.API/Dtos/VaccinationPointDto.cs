﻿namespace POMain.API.Dtos
{
    public class VaccinationPointDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        public string Principal { get; set; }
    }
}
