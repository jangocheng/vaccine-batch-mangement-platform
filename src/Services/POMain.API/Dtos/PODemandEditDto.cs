﻿

namespace POMain.API.Dtos
{
    public class PODemandEditDto
    {


        /// <summary>
        /// 需求单号
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 接种点ID
        /// </summary>
        public virtual Guid? VaccinationPointId { get; set; }


        /// <summary>
        /// 生产厂家ID
        /// </summary>
        public virtual Guid? ManufactorId { get; set; }


        /// <summary>
        /// 需求日期
        /// </summary>
        public virtual DateTime DemandDate { get; set; }

        /// <summary>
        /// 疫苗需求明细
        /// </summary>
        public ICollection<PODetailsEditDto> Items { get; set; }
    }
}
