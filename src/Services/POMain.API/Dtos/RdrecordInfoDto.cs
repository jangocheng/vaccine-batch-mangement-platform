﻿using API.Enums;


namespace POMain.API.Dtos
{
    public class RdrecordInfoDto
    {

        #region 【明细表ID】

        /// <summary>
        /// 明细ID
        /// </summary>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// 主表ID
        /// </summary>
        public virtual Guid RdrecordId { get; set; }

        /// <summary>
        /// 采购需求明细ID
        /// </summary>
        public virtual Guid? PODetailsId { get; set; }

        /// <summary>
        /// 疫苗ID
        /// </summary>
        public virtual Guid VaccineId { get; set; }

        /// <summary>
        /// 收货数量
        /// </summary>
        public virtual decimal Quantity { get; set; }


        /// <summary>
        /// 批次号
        /// </summary>
        public virtual string BatchCode { get; set; }

        /// <summary>
        /// 疫苗SN
        /// </summary>
        public virtual string SN { get; set; }

        /// <summary>
        /// 生产日期
        /// </summary>
        public virtual DateTime? MNDate { get; set; }

        /// <summary>
        /// 有效期
        /// </summary>
        public virtual DateTime? ValidityDate { get; set; }
        #endregion

        #region  【主表】

        /// <summary>
        /// 收发类型
        /// </summary>
        public virtual RdrecordTypeEnum RdrecordType { get;  set; }

        /// <summary>
        /// 接种站点ID
        /// </summary>
        public virtual Guid VaccinationPointId { get;  set; }

        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get;  set; }

        /// <summary>
        /// 单据日期
        /// </summary>
        public virtual DateTime VoucherDate { get;  set; }

        /// <summary>
        /// 生产厂家ID
        /// </summary>
        public virtual Guid ManufactorId { get;  set; }


        #endregion


        /// <summary>
        /// 疫苗名称
        /// </summary>
        public virtual string VaccineName { get; set; }
        /// <summary>
        /// 疫苗编号
        /// </summary>
        public virtual string VaccineCode { get; set; }


        /// <summary>
        /// 生产厂家
        /// </summary>
        public virtual string CompanyName { get; set; }


        /// <summary>
        /// 接种点名称
        /// </summary>
        public virtual string VaccinationPointName { get; set; }
    }
}
