﻿namespace POMain.API.Dtos
{
    public class PODetailsEditDto
    {

        /// <summary>
        /// KEY
        /// </summary>
        public virtual Guid? Id { get; set; }

        /// <summary>
        /// 需求主表ID
        /// </summary>
        public virtual Guid? POMainId { get; set; }

        /// <summary>
        /// 疫苗ID
        /// </summary>
        public virtual Guid? VaccineId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public virtual decimal Quantity { get; set; }

        /// <summary>
        /// 累计收货数量
        /// </summary>
        public virtual decimal TotalReceiveQty { get; set; }
    }
}
