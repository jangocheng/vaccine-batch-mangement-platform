﻿


数据库迁移命令


1. 开启迁移
   
Enable-Migrations -EnableAutomaticMigrations

2. 生成迁移命令

Add-Migration InitialCreate

多数据库指定执行
//Add-Migration DanikorSystemInitialCreate -context DanikorSystemDbContext
Add-Migration ProjectManagementInitialCreate -context ProjectManagementDbContext

3. 更新数据库

Update-Database -Verbose



多数据库指定执行
//Update-Database -Verbose -context DanikorSystemDbContext
Update-Database -Verbose -context ProjectManagementDbContext