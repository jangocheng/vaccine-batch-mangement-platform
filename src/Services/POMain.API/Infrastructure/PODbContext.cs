﻿using API.Entities;
using API.EntityConfigurations;
using Microsoft.EntityFrameworkCore;



namespace POMain.API.Infrastructure
{
    public class PODbContext : DbContext
    {
        public PODbContext(DbContextOptions<PODbContext> options)
            : base(options)
        {
        }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Vaccine> Vaccines { get; set; }
        public DbSet<VaccinationPoint> VaccinationPoints { get; set; }
        public DbSet<PODemand> PODemands  { get; set; }
        public DbSet<PODetails> PODetails { get; set; }

        public DbSet<Rdrecord> Rdrecords { get; set; }
        public DbSet<RdrecordDetails> RdrecordDetailss { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new PODemandConfiguration());
            builder.ApplyConfiguration(new PODetailsConfiguration());
            builder.ApplyConfiguration(new CompanyConfiguration());
            builder.ApplyConfiguration(new VaccineConfiguration());
            builder.ApplyConfiguration(new VaccinationPointConfiguration());

            builder.ApplyConfiguration(new RdrecordConfiguration());
            builder.ApplyConfiguration(new RdrecordDetailsConfiguration());
        }     
    }
}
