﻿using API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace API.EntityConfigurations
{

    public class RdrecordConfiguration : IEntityTypeConfiguration<Rdrecord>
    {
        public void Configure(EntityTypeBuilder<Rdrecord> builder)
        {
            builder.ToTable("ST_Rdrecord");
            builder.HasKey(v => v.Id);


        }
    }
}
