﻿using API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace API.EntityConfigurations
{


    public class RdrecordDetailsConfiguration : IEntityTypeConfiguration<RdrecordDetails>
    {
        public void Configure(EntityTypeBuilder<RdrecordDetails> builder)
        {
            builder.ToTable("ST_RdrecordDetails");
            builder.HasKey(v => v.Id);
        }
    }
}
