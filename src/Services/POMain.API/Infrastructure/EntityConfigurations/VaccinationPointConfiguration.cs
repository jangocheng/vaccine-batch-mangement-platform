﻿using API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace API.EntityConfigurations
{
    public class VaccinationPointConfiguration : IEntityTypeConfiguration<VaccinationPoint>
    {
        public void Configure(EntityTypeBuilder<VaccinationPoint> builder)
        {
            builder.ToTable("AA_VaccinationPoint");

            builder.HasKey(v => v.Id);


        }
    }
}
