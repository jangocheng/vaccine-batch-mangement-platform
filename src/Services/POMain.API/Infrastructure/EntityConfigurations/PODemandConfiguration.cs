﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using API.Entities;

namespace API.EntityConfigurations
{
    public class PODemandConfiguration : IEntityTypeConfiguration<PODemand>
    {
        public void Configure(EntityTypeBuilder<PODemand> builder)
        {
            builder.ToTable("PC_PODemand");

            builder.HasKey(v => v.Id);


            builder.Property(v => v.Code)
                .IsRequired()
                .HasMaxLength(60);
        }
    }
}
