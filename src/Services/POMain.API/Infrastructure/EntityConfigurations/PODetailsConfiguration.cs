﻿using API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace API.EntityConfigurations
{
    public class PODetailsConfiguration : IEntityTypeConfiguration<PODetails>
    {
        public void Configure(EntityTypeBuilder<PODetails> builder)
        {
            builder.ToTable("PC_PODetails");

            builder.Property(item => item.Quantity)
                .HasPrecision(14, 6)
                .IsRequired(true);
        }
    }
}
