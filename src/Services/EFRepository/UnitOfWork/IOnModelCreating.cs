﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messhall.Framework.Autofac;

namespace Messhall.Framework.Repository.UnitOfWork
{
    public interface IOnModelCreating : IDependency
    {
        void OnModelCreating(ModelBuilder modelBuilder);
    }
}
