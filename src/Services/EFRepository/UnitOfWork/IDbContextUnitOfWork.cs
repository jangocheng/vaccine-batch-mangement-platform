﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messhall.Framework.Domain.DomainObject;
using Messhall.Framework.Domain.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace Messhall.Framework.Repository.UnitOfWork
{
    public interface IDbContextUnitOfWork : IAppUnitOfWork
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : EntityBase;
    }
}
