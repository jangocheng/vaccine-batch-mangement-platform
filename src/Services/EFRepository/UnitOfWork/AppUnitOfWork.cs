﻿using Messhall.Framework.Domain.DomainObject;
using Messhall.Framework.Domain.UnitOfWork;
using Messhall.Framework.Repository.Repositorys;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Messhall.Framework.Autofac;

namespace Messhall.Framework.Repository.UnitOfWork
{
    public class AppUnitOfWork : DbContext, IDbContextUnitOfWork
    {
        public AppUnitOfWork(DbContextOptions options) : base(options)
        {
            
        }

        public new DbSet<TEntity> Set<TEntity>() where TEntity : EntityBase
        {
            return base.Set<TEntity>();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var onModelCreatings = AutofacBuilder.ResolveAll<IOnModelCreating>();

            foreach (var item in onModelCreatings)
            {
                Console.WriteLine(item);
                item.OnModelCreating(modelBuilder);
            }

            base.OnModelCreating(modelBuilder);
        }

        public async Task BeginTransactionAsync(CancellationToken cancellationToken = default)
        {
            await this.Database.BeginTransactionAsync(cancellationToken);
        }

        public async Task CommitAsync(CancellationToken cancellationToken = default)
        {
            await this.Database.CommitTransactionAsync(cancellationToken);
        }

        public async Task RollbackAsync(CancellationToken cancellationToken = default)
        {
            await this.Database.RollbackTransactionAsync(cancellationToken);
        }
    }
}
