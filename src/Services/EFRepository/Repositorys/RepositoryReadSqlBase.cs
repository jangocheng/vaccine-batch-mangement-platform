﻿using Messhall.Framework.Domain.Repository;
using Messhall.Framework.Repository.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messhall.Framework.EntityFrameworkCore;

namespace Messhall.Framework.Repository.Repositorys
{
    /// <summary>
    /// 只做查询一系列操作
    /// </summary>
    public class RepositoryReadSqlBase : IRepositoryReadSqlBase
    {
        protected readonly DbContext _dbContext;

        public RepositoryReadSqlBase(IDbContextUnitOfWork dbContext)
        {
            _dbContext = (DbContext)dbContext;
        }

        /// <summary>
        /// Sql方式查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IList<T>> QueryAsync<T>(string sql, params object[] parameters) where T : class, new()
        {
            return await _dbContext.Database.SqlQueryAsync<T>(sql, parameters);
        }

        /// <summary>
        /// Sql方式查询-分页
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<(IList<T>, int)> PagingAsync<T>( string sqlField, string sqlFromOrWhere, int? pageIndex, int? pageSize, (string sortName, bool sort) sortCriteria, params object[] parameters) where T : class, new()
        {
            return await _dbContext.Database.PagingAsync<T>(sqlField, sqlFromOrWhere, pageIndex, pageSize, sortCriteria,parameters);
        }
    }
}
