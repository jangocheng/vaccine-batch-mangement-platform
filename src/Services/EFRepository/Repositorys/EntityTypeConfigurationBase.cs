﻿using Messhall.Framework.Domain;
using Messhall.Framework.Domain.DomainObject;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messhall.Framework.Repository.Repositorys
{
    public class EntityTypeConfigurationBase<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : EntityBoBase
    {
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.HasKey(c => c.KeyId);
            builder.Property(c => c.CreateTime).IsRequired();
            builder.Property(c => c.CreateName).HasMaxLength(50).HasDefaultValue(string.Empty);
            builder.Property(c => c.UpdateTime).IsRequired();
            builder.Property(c => c.UpdateName).HasMaxLength(50).HasDefaultValue(string.Empty);
            builder.Property(c => c.RowVersion).IsRowVersion().IsConcurrencyToken().IsRequired();
            builder.Property(c => c.IsDelete).HasDefaultValue(false).IsRequired();  
            
            builder.HasQueryFilter(c => c.IsDelete == false);
        }
    }
}