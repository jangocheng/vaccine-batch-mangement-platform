﻿using Identity.Models;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Identity.Domain.Infrastructure.contexts;
public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.Entity<IdentityRole>().HasData(
            new IdentityRole(){
                Id="b5550ce8-2341-4b3e-99b5-c76ada00f6ff",
                Name="Manufacturer",
                NormalizedName="MANUFACTURER",
                ConcurrencyStamp="2f0e9dd7-7a39-452a-8c7a-c0b3ed3f88a6"
            },
            new IdentityRole(){
                Id="043bb85f-bcd1-4822-9d0e-11a758045354",
                Name="Admin",
                NormalizedName="ADMIN",
                ConcurrencyStamp="043bb85f-bcd1-4822-9d0e-11a758045354"
            },
            new IdentityRole(){
                Id="ac21fe6c-1dcb-4883-abbe-2b437b237714",
                Name="OnSite",
                NormalizedName="ONSITE",
                ConcurrencyStamp="a5caf50a-f938-4962-8f6b-1608a04c8132"
            }
        );
        // Customize the ASP.NET Identity model and override the defaults if needed.
        // For example, you can rename the ASP.NET Identity table names and more.
        // Add your customizations after calling base.OnModelCreating(builder);
    }
}
