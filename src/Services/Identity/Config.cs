﻿using IdentityServer4;
using IdentityServer4.Models;

public class Config
{
    public static IEnumerable<IdentityResource> IdentityResources =>
        new IdentityResource[]
        {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
        };

    public static IEnumerable<ApiScope> ApiScopes =>
        new ApiScope[]
        {
                new ApiScope("Batch", "Access to Batch API"),
                new ApiScope("Site", "Access to Site API"),
        };

    public static IEnumerable<ApiResource> ApiResources =>
        new ApiResource[]
        {
                new ApiResource("batch-api", "Batch API")
                {
                    Scopes = { "Batch" }

                },
                new ApiResource("site-api", "Site API")
                {
                    Scopes = { "Site" }
                }
        };

    public static IEnumerable<Client> GetClients(IConfiguration configuration)
    {
        return new List<Client>
            {
                new Client
                {
                    ClientId = "blazor",
                    ClientName = "Blazor Front-end",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    RequireClientSecret = false,
                    
                    RequireConsent = false,

                    AllowedCorsOrigins = { "https://localhost"},

                    // where to redirect to after login
                    RedirectUris = { "https://localhost:7246/authentication/login-callback" },

                    // where to redirect to after logout
                    PostLogoutRedirectUris =
                    {
                        $"{configuration["BlazorClientUrlExternal"]}/authentication/logout-callback"
                    },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "Batch",
                        "Site"
                    },
                }
    };
    }
}
