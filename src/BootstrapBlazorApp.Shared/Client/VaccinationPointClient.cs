﻿using BootstrapBlazorApp.Shared.Models.VaccinationPoint;
using System.Net.Http.Json;

namespace BootstrapBlazorApp.Shared.Client
{

    public class VaccinationPointClient
    {


        private readonly HttpClient httpClient;

        public VaccinationPointClient(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<VaccinationPointModel>> GetAllListAsync()
        {
            var dataList = await httpClient.GetFromJsonAsync<IEnumerable<VaccinationPointModel>>(
"api/v1/vaccinationPoint/all");

            return dataList;

        }

        public async Task CreateAsync(VaccinationPointModel input)
        {
            await httpClient.PostAsJsonAsync<VaccinationPointModel>("api/v1/vaccinationPoint/create", input);
        }

        public async Task UpdateAsync(Guid id,VaccinationPointModel input)
        {
            await httpClient.PostAsJsonAsync<VaccinationPointModel>("api/v1/vaccinationPoint/update?id="+id, input);
        }

        public async Task DeleteAsync(Guid id)
        {
            await httpClient.DeleteAsync("api/v1/vaccinationPoint/delete?id="+id);
        }

    }
}
