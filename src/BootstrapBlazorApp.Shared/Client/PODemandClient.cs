﻿using BootstrapBlazorApp.Shared.Models.POMain;
using Common.ViewModel;
using System.Net.Http.Json;

namespace BootstrapBlazorApp.Shared.Client
{

    public class PODemandClient
    {


        private readonly HttpClient httpClient;

        public PODemandClient(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<PaginatedItemsViewModel<POInfoListModel>> GetPagedPOInfoAsync(string? keyword = "",
             int pageSize = 10,
             int pageIndex = 0)
        {
            var result = await httpClient.GetFromJsonAsync<PaginatedItemsViewModel<POInfoListModel>>(
String.Format("api/v1/PODemand/paged/poinfo?keyword={0}&pageSize={1}&pageIndex={2}", keyword, pageSize, pageIndex));

            return result;

        }

        public async Task CreateAsync(POMainModel input)
        {
            var saveResult =  await httpClient.PostAsJsonAsync<POMainModel>("api/v1/PODemand/create", input);
        }

        public async Task UpdateAsync(Guid id, POMainModel input)
        {
            await httpClient.PostAsJsonAsync<POMainModel>("api/v1/PODemand/update?id=" + id, input);
        }

        public async Task DeleteItemAsync(Guid id)
        {
            await httpClient.DeleteAsync("api/v1/PODemand/deleteItem?id=" + id);
        }


       

    }
}
