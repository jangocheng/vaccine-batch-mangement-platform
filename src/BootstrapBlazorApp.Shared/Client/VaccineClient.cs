﻿using BootstrapBlazorApp.Shared.Models;
using BootstrapBlazorApp.Shared.Models.VaccinationPoint;
using System.Net.Http.Json;

namespace BootstrapBlazorApp.Shared.Client
{

    public class VaccineClient
    {


        private readonly HttpClient httpClient;

        public VaccineClient(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<VaccineModel>> GetAllListAsync()
        {
            var dataList = await httpClient.GetFromJsonAsync<IEnumerable<VaccineModel>>(
"api/v1/vaccine/all");

            return dataList;

        }

        public async Task CreateAsync(VaccineModel input)
        {
            await httpClient.PostAsJsonAsync<VaccineModel>("api/v1/vaccine/create", input);
        }

        public async Task UpdateAsync(Guid id, VaccineModel input)
        {
            await httpClient.PostAsJsonAsync<VaccineModel>("api/v1/vaccine/update?id=" + id, input);
        }

        public async Task DeleteAsync(Guid id)
        {
            await httpClient.DeleteAsync("api/v1/vaccine/delete?id=" + id);
        }

    }
}
