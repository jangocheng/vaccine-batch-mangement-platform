﻿using BootstrapBlazor.Components;
using BootstrapBlazorApp.Shared.Data;
using BootstrapBlazorApp.Shared.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http.Json;

namespace BootstrapBlazorApp.Shared.Pages
{
    public partial class VaccineBatchRecord
    {
        [Inject]
        [NotNull]
        private IStringLocalizer<VaccineBatchRecordModel>? Localizer { get; set; }

        [Inject]
        [NotNull]
        private DialogService? DialogService { get; set; }

        [Inject]
        [NotNull]
        private VaccineBatchRecordService vaccineBatchRecordService { get; set; }

        [Inject]
        [NotNull]
        private HttpClient Http { get; set; }



        private static readonly Random random = new();
        /// <summary>
        /// 获得/设置 分页配置数据源
        /// </summary>
        private static IEnumerable<int> PageItemsSource => new int[] { 10 };

        private static string GetAvatarUrl(int id) => $"_content/BootstrapBlazorApp.Shared/images/avatars/150-{id}.jpg";

        private static Color GetProgressColor(int count) => count switch
        {
            >= 0 and < 10 => Color.Secondary,
            >= 10 and < 20 => Color.Danger,
            >= 20 and < 40 => Color.Warning,
            >= 40 and < 50 => Color.Info,
            >= 50 and < 70 => Color.Primary,
            _ => Color.Success
        };
        private VaccineBatchRecordModel Model { get; set; } = new VaccineBatchRecordModel()
        {
        };

        private List<SelectedItem> vaccineModels = new List<SelectedItem>();
        /// <summary>
        /// OnInitialized 方法
        /// </summary>
        protected override async Task OnInitializedAsync()
        {
            var res = await vaccineBatchRecordService.GetVaccineModels();
            vaccineModels= new List<SelectedItem>() { new SelectedItem() { Value= "8dd42405-502d-4789-a07e-25cf6ca37828" ,Text= "M002(科泰生物)" }, new SelectedItem() { Value = "8dd42405-502d-4789-a07e-25cf6ca37829", Text = "M001(国兴制药)" } };
            await base.OnInitializedAsync();
        }

        [NotNull]
        private IEnumerable<VaccineBatchRecordModel>? Items { get; set; }

        private static readonly ConcurrentDictionary<Type, Func<IEnumerable<VaccineBatchRecordModel>, string, SortOrder, IEnumerable<VaccineBatchRecordModel>>> SortLambdaCache = new();

        private async Task<QueryData<VaccineBatchRecordModel>> OnQueryAsync(QueryPageOptions options)
        {            
            var items = await vaccineBatchRecordService.GetListAsync(options.SearchText ?? "");

            var isSearched = false;
           
            // 过滤
            var isFiltered = false;

            // 排序
            var isSorted = false;
            
            var total = items.Count();

            return await Task.FromResult(new QueryData<VaccineBatchRecordModel>()
            {
                Items = items.Skip((options.PageIndex - 1) * options.PageItems).Take(options.PageItems).ToList(),
                TotalCount = total,
                IsFiltered = isFiltered,
                IsSorted = isSorted,
                IsSearch = isSearched
            });
        }

        

        private async Task<bool> OnSaveAsync(VaccineBatchRecordModel item, ItemChangedType changedType)
        {
            // 增加数据演示代码
            if (changedType == ItemChangedType.Add)
            {
                await vaccineBatchRecordService.Add(item);
            }
            else
            {
                await vaccineBatchRecordService.Update(item);
            }
            return await Task.FromResult(true);
        }

        private async Task<bool> OnDeleteAsync(IEnumerable<VaccineBatchRecordModel> items)
        {
            var id = items.FirstOrDefault()?.Id;
            if (id == null) return false;
            await vaccineBatchRecordService.Delete(id.Value);
            return await Task.FromResult(true);
        }

        private async Task ShowDialog()
        {
            var items = Utility.GenerateEditorItems<VaccineBatchRecordModel>();


            var option = new EditDialogOption<VaccineBatchRecordModel>()
            {
                Title = "编辑疫苗批次",
                Model = Model,
                Items = items,
                ItemsPerRow = 2,
                RowType = RowType.Inline,
                OnCloseAsync = () =>
                {
                    return Task.CompletedTask;
                },
                OnEditAsync = async context =>
                {
                    //获取context.Model 进行保存入库
                    await vaccineBatchRecordService.Add(context.Model as VaccineBatchRecordModel);
                    return await Task.FromResult(true);
                }
            };

            await DialogService.ShowEditDialog(option);
        }

        private async Task ShowEditDialog()
        {
            var items = Utility.GenerateEditorItems<VaccineBatchRecordModel>();


            var option = new EditDialogOption<VaccineBatchRecordModel>()
            {
                Title = "编辑对话框",
                Model = Model,
                Items = items,
                ItemsPerRow = 2,
                RowType = RowType.Inline,
                OnCloseAsync = () =>
                {
                    return Task.CompletedTask;
                },
                OnEditAsync = context =>
                {
                    return Task.FromResult(true);
                }
            };

            await DialogService.ShowEditDialog(option);
        }
    }
}
