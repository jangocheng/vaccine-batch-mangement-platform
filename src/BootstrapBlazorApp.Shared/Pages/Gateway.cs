﻿using BootstrapBlazor.Components;

using BootstrapBlazorApp.Shared.Data;
using BootstrapBlazorApp.Shared.Models;

using Microsoft.AspNetCore.Components;

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace BootstrapBlazorApp.Shared.Pages
{
    public partial class Gateway 
    {
        private List<CompanyModel> companyModel;
        [Inject]
        [NotNull]
        private GatewayService gatewayService { get; set; }

        private async Task RequestData()
        {
            var result = await gatewayService.GetCompaniesAsync();

            companyModel = result?.ToList();
        }
    }

}
