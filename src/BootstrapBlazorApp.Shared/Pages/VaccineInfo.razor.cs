﻿using BootstrapBlazor.Components;
using BootstrapBlazorApp.Shared.Data;
using BootstrapBlazorApp.Shared.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http.Json;

namespace BootstrapBlazorApp.Shared.Pages
{
    public partial class VaccineInfo
    {
        [Inject]
        [NotNull]
        private IStringLocalizer<VaccineModel>? Localizer { get; set; }

        [Inject]
        [NotNull]
        private DialogService? DialogService { get; set; }

        [Inject]
        [NotNull]
        private VaccineService vaccineService { get; set; }

        [Inject]
        [NotNull]
        private HttpClient Http { get; set; }

        private static readonly Random random = new();
        /// <summary>
        /// 获得/设置 分页配置数据源
        /// </summary>
        private static IEnumerable<int> PageItemsSource => new int[] { 10 };

        private static string GetAvatarUrl(int id) => $"_content/BootstrapBlazorApp.Shared/images/avatars/150-{id}.jpg";

        private static Color GetProgressColor(int count) => count switch
        {
            >= 0 and < 10 => Color.Secondary,
            >= 10 and < 20 => Color.Danger,
            >= 20 and < 40 => Color.Warning,
            >= 40 and < 50 => Color.Info,
            >= 50 and < 70 => Color.Primary,
            _ => Color.Success
        };
        private VaccineModel Model { get; set; } = new VaccineModel()
        {

        };
        private List<CompanyModel> companyModels = new List<CompanyModel>();
        /// <summary>
        /// OnInitialized 方法
        /// </summary>
        protected override async Task OnInitializedAsync()
        {
            companyModels = await vaccineService.GetCompanyModels();
            await base.OnInitializedAsync();
        }

        [NotNull]
        private IEnumerable<VaccineModel>? Items { get; set; }

        private static readonly ConcurrentDictionary<Type, Func<IEnumerable<VaccineModel>, string, SortOrder, IEnumerable<VaccineModel>>> SortLambdaCache = new();

        private async Task<QueryData<VaccineModel>> OnQueryAsync(QueryPageOptions options)
        {           
            //方式二
            var Items = await vaccineService.GetList(options.SearchText ?? "");                        
            var items = Items;
            var isSearched = false;
            var total = items.Count();
            return await Task.FromResult(new QueryData<VaccineModel>()
            {
                Items = items.Skip((options.PageIndex - 1) * options.PageItems).Take(options.PageItems).ToList(),
                TotalCount = total,
                IsFiltered = false,
                IsSorted = true,
                IsSearch = isSearched
            });
        }
        private async Task<bool> OnSaveAsync(VaccineModel item, ItemChangedType changedType)
        {
            // 增加数据演示代码
            if (changedType == ItemChangedType.Add)
            {
                await vaccineService.AddVaccine(item);
            }
            else
            {
                await vaccineService.UpdateVaccine(item);
            }
            return await Task.FromResult(true);
        }

        private async Task<bool> OnDeleteAsync(IEnumerable<VaccineModel> items)
        {
            var id = items.FirstOrDefault()?.Id;
            if (id == null) return false;
            await vaccineService.DeleteVaccine(id.Value);
            return await Task.FromResult(true);
        }

        private IEnumerable<SelectedItem> Getcompanys(VaccineModel item)
        {
            return companyModels.Select(x => new SelectedItem() { Text = x.CompanyName, Value = x.Id.ToString() });
        }
        
        private async Task ShowDialog()
        {
            var items = Utility.GenerateEditorItems<VaccineModel>();


            var option = new EditDialogOption<VaccineModel>()
            {
                Title = "编辑对话框",
                Model = Model,
                Items = items,
                ItemsPerRow = 2,
                RowType = RowType.Inline,
                OnCloseAsync = () =>
                {
                    return Task.CompletedTask;
                },
                OnEditAsync = async context =>
                {
                    //获取context.Model 进行保存入库
                    await vaccineService.AddVaccine(context.Model as VaccineModel);
                    return await Task.FromResult(true);
                }
            };

            await DialogService.ShowEditDialog(option);
        }

        private async Task ShowEditDialog()
        {
            var items = Utility.GenerateEditorItems<VaccineModel>();


            var option = new EditDialogOption<VaccineModel>()
            {
                Title = "编辑对话框",
                Model = Model,
                Items = items,
                ItemsPerRow = 2,
                RowType = RowType.Inline,
                OnCloseAsync = () =>
                {
                    return Task.CompletedTask;
                },
                OnEditAsync = context =>
                {
                    return Task.FromResult(true);
                }
            };

            await DialogService.ShowEditDialog(option);
        }
    }
}
