﻿using BootstrapBlazor.Components;
using BootstrapBlazorApp.Shared.Data;
using BootstrapBlazorApp.Shared.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;

namespace BootstrapBlazorApp.Shared.Pages
{
    public partial class VaccineDeliveryRecord
    {
        [Inject]
        [NotNull]
        private IStringLocalizer<VaccineDeliveryRecordModel>? Localizer { get; set; }

        [Inject]
        [NotNull]
        private DialogService? DialogService { get; set; }

        [Inject]
        [NotNull]
        private VaccineDeliveryRecordService vaccineDeliveryRecordService { get; set; }

        [Inject]
        [NotNull]
        private HttpClient httpClient { get; set; }

        private static readonly Random random = new();
        /// <summary>
        /// 获得/设置 分页配置数据源
        /// </summary>
        private static IEnumerable<int> PageItemsSource => new int[] { 10 };

        private static string GetAvatarUrl(int id) => $"_content/BootstrapBlazorApp.Shared/images/avatars/150-{id}.jpg";

        private static Color GetProgressColor(int count) => count switch
        {
            >= 0 and < 10 => Color.Secondary,
            >= 10 and < 20 => Color.Danger,
            >= 20 and < 40 => Color.Warning,
            >= 40 and < 50 => Color.Info,
            >= 50 and < 70 => Color.Primary,
            _ => Color.Success
        };
        private VaccineDeliveryRecordModel Model { get; set; } = new VaccineDeliveryRecordModel()
        {
        };

        [NotNull]
        private IEnumerable<VaccineDeliveryRecordModel>? Items { get; set; }

        private static readonly ConcurrentDictionary<Type, Func<IEnumerable<VaccineDeliveryRecordModel>, string, SortOrder, IEnumerable<VaccineDeliveryRecordModel>>> SortLambdaCache = new();

        private async Task<QueryData<VaccineDeliveryRecordModel>> OnQueryAsync(QueryPageOptions options)
        {
            var items = await vaccineDeliveryRecordService.GetListAsync(options.SearchText ?? "");

            var isSearched = false;

            // 过滤
            var isFiltered = false;

            // 排序
            var isSorted = false;

            var total = items.Count();

            return await Task.FromResult(new QueryData<VaccineDeliveryRecordModel>()
            {
                Items = items.Skip((options.PageIndex - 1) * options.PageItems).Take(options.PageItems).ToList(),
                TotalCount = total,
                IsFiltered = isFiltered,
                IsSorted = isSorted,
                IsSearch = isSearched
            });
        }

        private async Task ShowDialog()
        {
            var items = Utility.GenerateEditorItems<VaccineDeliveryRecordModel>();

            var option = new EditDialogOption<VaccineDeliveryRecordModel>()
            {
                Title = "编辑对话框",
                Model = Model,
                Items = items,
                ItemsPerRow = 2,
                RowType = RowType.Inline,
                OnCloseAsync = () =>
                {
                    return Task.CompletedTask;
                },
                OnEditAsync = async context =>
                {
                    //获取context.Model 进行保存入库
                    await vaccineDeliveryRecordService.Add(context.Model as VaccineDeliveryRecordModel);
                    return true;
                }
            };

            await DialogService.ShowEditDialog(option);
        }

        private async Task<bool> OnSaveAsync(VaccineDeliveryRecordModel item, ItemChangedType changedType)
        {
            // 增加数据演示代码
            if (changedType == ItemChangedType.Add)
            {
                await vaccineDeliveryRecordService.Add(item);
            }
            else
            {
                await vaccineDeliveryRecordService.Update(item);
            }
            return await Task.FromResult(true);
        }

        private async Task<bool> OnDeleteAsync(IEnumerable<VaccineDeliveryRecordModel> items)
        {
            var id = items.FirstOrDefault()?.Id;
            if (id == null) return false;
            await vaccineDeliveryRecordService.Delete(id.Value);
            return await Task.FromResult(true);
        }


        private async Task ShowEditDialog()
        {
            var items = Utility.GenerateEditorItems<VaccineDeliveryRecordModel>();


            var option = new EditDialogOption<VaccineDeliveryRecordModel>()
            {
                Title = "编辑对话框",
                Model = Model,
                Items = items,
                ItemsPerRow = 2,
                RowType = RowType.Inline,
                OnCloseAsync = () =>
                {
                    return Task.CompletedTask;
                },
                OnEditAsync = context =>
                {
                    return Task.FromResult(true);
                }
            };

            await DialogService.ShowEditDialog(option);
        }
    }
}
