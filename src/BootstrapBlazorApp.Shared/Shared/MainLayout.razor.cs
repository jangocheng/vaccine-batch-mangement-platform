﻿using BootstrapBlazor.Components;
using Microsoft.AspNetCore.Components.Routing;

namespace BootstrapBlazorApp.Shared.Shared
{
    /// <summary>
    ///
    /// </summary>
    public sealed partial class MainLayout
    {
        private bool UseTabSet { get; set; } = true;

        private string Theme { get; set; } = "";

        private bool IsOpen { get; set; }

        private bool IsFixedHeader { get; set; } = true;

        private bool IsFixedFooter { get; set; } = true;

        private bool IsFullSide { get; set; } = true;

        private bool ShowFooter { get; set; } = true;

        private List<MenuItem>? Menus { get; set; }

        /// <summary>
        /// OnInitialized 方法
        /// </summary>
        protected override void OnInitialized()
        {
            base.OnInitialized();

            Menus = GetIconSideMenuItems();
        }

        private static List<MenuItem> GetIconSideMenuItems()
        {
            var menus = new List<MenuItem>
            {
                new MenuItem() { Text = "厂家配置", Icon = "fa fa-fw fa-home", Url = "/company",
                    Items=new List<MenuItem>(){
                    new MenuItem() { Text = "疫苗档案", Icon = "fa fa-fw fa-home", Url = "/vaccine" },
                    new MenuItem() { Text = "疫苗批次档案", Icon = "fa fa-fw fa-fa", Url = "/vaccineBatchRecord" , Match = NavLinkMatch.All},
                    new MenuItem() { Text = "疫苗发货", Icon = "fa fa-fw fa-fa", Url = "/vaccinedeliveryrecord" , Match = NavLinkMatch.All},
                      new MenuItem() { Text = "统计", Icon = "fa fa-fw fa-fa", Url = "/statistics" , Match = NavLinkMatch.All}} },

                new MenuItem() { Text = "接种点", Icon = "fa fa-fw fa-check-square-o", Url = "/counter" ,Items=new List<MenuItem>(){
                    new MenuItem() { Text = "需求采购", Icon = "fa fa-fw fa-home", Url = "/pomain/list" },
                    new MenuItem() { Text = "收货", Icon = "fa fa-fw fa-fa", Url = "/" , Match = NavLinkMatch.All},
                    new MenuItem() { Text = "接种登记", Icon = "fa fa-fw fa-fa", Url = "/" , Match = NavLinkMatch.All},
                    new MenuItem() { Text = "统计", Icon = "fa fa-fw fa-fa", Url = "/" , Match = NavLinkMatch.All}}},
                new MenuItem() { Text = "系统配置", Icon = "fa fa-fw fa-database", Url = "fetchdata" ,Items=new List<MenuItem>(){
                    new MenuItem() { Text = "网关测试", Icon = "fa fa-fw fa-home", Url = "/gateway" },
                    new MenuItem() { Text = "用户", Icon = "fa fa-fw fa-home", Url = "/users" },
                    new MenuItem() { Text = "角色", Icon = "fa fa-fw fa-fa", Url = "/" , Match = NavLinkMatch.All},
                    new MenuItem() { Text = "权限", Icon = "fa fa-fw fa-fa", Url = "/" , Match = NavLinkMatch.All},
                    new MenuItem() { Text = "疫苗厂家", Icon = "fa fa-fw fa-fa", Url = "/company" , Match = NavLinkMatch.All},
                    new MenuItem() { Text = "接种站点", Icon = "fa fa-fw fa-fa", Url = "/vaccinationPoint/list" , Match = NavLinkMatch.All},
                new MenuItem() { Text = "疫苗批次管理平台", Icon = "fa fa-fw fa-fa", Url = "/" , Match = NavLinkMatch.All}}}
            };

            return menus;
        }
    }
}
