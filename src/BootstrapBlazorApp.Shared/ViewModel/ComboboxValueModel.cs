﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.ViewModel
{
    public class ComboboxValueModel<T>
    {
        public virtual T Value { get; set; }

        public virtual string Text { get; set; }
    }
}
