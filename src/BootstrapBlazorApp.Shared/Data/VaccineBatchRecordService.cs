﻿using BootstrapBlazor.Components;
using BootstrapBlazorApp.Shared.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Data
{
    public class VaccineBatchRecordService : BaseService 
    {
        private readonly IHttpClientFactory _clientFactory;

        public VaccineBatchRecordService(IHttpClientFactory clientFactory) : base(clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<List<VaccineBatchRecordModel>> GetListAsync(string Name)
        {
            var result = new List<VaccineBatchRecordModel>();
            var url = string.Format("vaccineBatchRecord/{0}", Name);
            result = await HttpGet<List<VaccineBatchRecordModel>>(url);
            return result;           
        }

        public async Task<List<VaccineModel>> GetVaccineModels()
        {
            var url = string.Format("vaccineBatchRecord/vaccines");
            var result = await HttpGet<List<VaccineModel>>(url);
            return result;
        }

        public async Task<bool> Add(VaccineBatchRecordModel model)
        {
            var url = string.Format("vaccineBatchRecord/save");
            var result = await HttpPost<bool>(url,JsonConvert.SerializeObject(model));
            return true;
        }

        public async Task<bool> Update(VaccineBatchRecordModel model)
        {
            var url = string.Format("vaccineBatchRecord/update");
            var result = await HttpPut<bool>(url, JsonConvert.SerializeObject(model));
            return true;
        }

        public async Task<bool> Delete(Guid Id)
        {
            var url = string.Format("vaccineBatchRecord/remove/{0}",Id);
            var result = await HttpDelete<bool>(url);
            return true;
        }

    }
}
