﻿using BootstrapBlazor.Components;
using BootstrapBlazorApp.Shared.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Data
{
    public class VaccineService : BaseService 
    {
        private readonly IHttpClientFactory _clientFactory;

        public VaccineService(IHttpClientFactory clientFactory) : base(clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<List<VaccineModel>> GetList(string name)
        {
            var result = new List<VaccineModel>();
            var url = string.Format("Vaccine/{0}", name);
            result = await HttpGet<List<VaccineModel>>(url);
            return result;           
        }

        public async Task<bool> AddVaccine(VaccineModel model)
        {
            var url = string.Format("Vaccine/save");
            var result = await HttpPost<bool>(url,JsonConvert.SerializeObject(model));
            return true;
        }

        public async Task<bool> UpdateVaccine(VaccineModel model)
        {
            var url = string.Format("Vaccine/update");
            var result = await HttpPut<bool>(url, JsonConvert.SerializeObject(model));
            return true;
        }

        public async Task<bool> DeleteVaccine(Guid Id)
        {
            var url = string.Format("Vaccine/remove/{0}",Id);
            var result = await HttpDelete<bool>(url);
            return true;
        }

        public async Task<List<CompanyModel>> GetCompanyModels()
        {
            var url = string.Format("Vaccine/companys");
            var result = await HttpGet<List<CompanyModel>>(url);
            return result;
        }
        

    }
}
