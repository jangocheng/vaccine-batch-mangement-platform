﻿using BootstrapBlazor.Components;
using BootstrapBlazorApp.Shared.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Data
{
    public class VaccineDeliveryRecordService : BaseService 
    {
        private readonly IHttpClientFactory _clientFactory;

        public VaccineDeliveryRecordService(IHttpClientFactory clientFactory) : base(clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<List<VaccineDeliveryRecordModel>> GetListAsync(string Name)
        {
            var result = new List<VaccineDeliveryRecordModel>();
            var url = string.Format("vaccineDeliveryRecord/{0}", Name);
            result = await HttpGet<List<VaccineDeliveryRecordModel>>(url);
            return result;           
        }

        public async Task<bool> Add(VaccineDeliveryRecordModel model)
        {
            var url = string.Format("vaccineDeliveryRecord/save");
            var result = await HttpPost<bool>(url,JsonConvert.SerializeObject(model));
            return true;
        }

        public async Task<bool> Update(VaccineDeliveryRecordModel model)
        {
            var url = string.Format("vaccineDeliveryRecord/update");
            var result = await HttpPut<bool>(url, JsonConvert.SerializeObject(model));
            return true;
        }

        public async Task<bool> Delete(Guid Id)
        {
            var url = string.Format("vaccineDeliveryRecord/remove/{0}",Id);
            var result = await HttpDelete<bool>(url);
            return true;
        }

    }
}
