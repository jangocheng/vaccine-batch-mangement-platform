﻿using BootstrapBlazor.Components;
using BootstrapBlazorApp.Shared.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Data
{
    public class CompanyService : BaseService 
    {
        private readonly IHttpClientFactory _clientFactory;

        public CompanyService(IHttpClientFactory clientFactory) : base(clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<List<CompanyModel>> GetCompaniesAsync(string companyName)
        {
            var result = new List<CompanyModel>();
            var url = string.Format("company/{0}", companyName);
            result = await HttpGet<List<CompanyModel>>(url);
            return result;           
        }

        public async Task<bool> AddCompany(CompanyModel model)
        {
            var url = string.Format("company/save");
            var result = await HttpPost<bool>(url,JsonConvert.SerializeObject(model));
            return true;
        }

        public async Task<bool> UpdateCompany(CompanyModel model)
        {
            var url = string.Format("company/update");
            var result = await HttpPut<bool>(url, JsonConvert.SerializeObject(model));
            return true;
        }

        public async Task<bool> DeleteCompany(Guid Id)
        {
            var url = string.Format("company/remove/{0}",Id);
            var result = await HttpDelete<bool>(url);
            return true;
        }

    }
}
