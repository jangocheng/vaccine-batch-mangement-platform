﻿using BootstrapBlazor.Components;
using BootstrapBlazorApp.Shared.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Data
{
    public class GatewayService : BaseService 
    {
        private readonly IHttpClientFactory _clientFactory;

        public GatewayService(IHttpClientFactory clientFactory) : base(clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<List<CompanyModel>> GetCompaniesAsync()
        {
            var result = new List<CompanyModel>();
            var url = string.Format("https://localhost:5001/api/v1/company/all/{0}", "");
            result = await HttpGet<List<CompanyModel>>(url);
            return result;           
        }
    }
}
