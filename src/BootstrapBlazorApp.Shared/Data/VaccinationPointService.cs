﻿using BootstrapBlazorApp.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Data
{
    public class VaccinationPointService
    {
        public async Task<List<VaccinationPoineModel>> GetCompaniesAsync(string companyName)
        {
            List<VaccinationPoineModel> companies = new List<VaccinationPoineModel>()
            {
                new VaccinationPoineModel(){Name="上海闵行接种点", Address="上海闵行莘庄", Principal="jiesen" ,Phone="13691853296" ,Id=Guid.NewGuid() },
                new VaccinationPoineModel(){Name="上海浦东接种点", Address="上海浦东三林", Principal="maike" ,Phone="13691853298" ,Id=Guid.NewGuid()}
            };
            return await Task.FromResult(companies);
        }

        public async Task<bool> AddOrChangeCompany(CompanyModel model)
        {

            return true;
        }

        public async Task<bool> DeleteCompany(Guid Id)
        {


            return true;
        }
    }
}
