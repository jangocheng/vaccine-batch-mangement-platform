﻿using BootstrapBlazor.Components;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Models
{
    public class BaseModel
    {
        /// </summary>
        [Display(Name = "主键")]
        [AutoGenerateColumn(Ignore = true)]
        public Guid Id { get; set; }
    }
    public class CompanyModel : BaseModel
    {
        private static readonly Random random = new();

      

        [Required(ErrorMessage = "{0}不能为空")]
        [AutoGenerateColumn(Order = 10, Filterable = true, Searchable = true)]
        [Display(Name = "厂家名称")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "{0}不能为空")]
        [AutoGenerateColumn(Order = 10, Filterable = true, Searchable = true)]
        [Display(Name = "厂家地址")]
        public string Address { get; set; }
         
        [Display(Name = "联系方式")]
        public string Phone { get; set; }

        public static List<CompanyModel> GenerateCompanyModel(IStringLocalizer<CompanyModel> localizer, int count = 80) => Enumerable.Range(1, count).Select(i => new CompanyModel()
        {
            Id=Guid.NewGuid(),
            CompanyName = localizer["CompanyModel.CompanyName", $"{i:d4}"],
            Phone = localizer["CompanyModel.Phone", $"{random.Next(1000, 2000)}"],
            Address = localizer["CompanyModel.Address", $"{i:d4}"]           
        }).ToList();

       
    }
}
