﻿using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Models
{
    /// <summary>
    ///  接种点基础信息
    /// </summary>
    public class VaccinationPoineModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        public string Principal { get; set; }


        private static readonly Random random = new();
        public static List<VaccinationPoineModel> GenerateVaccinationPoineModel(IStringLocalizer<VaccinationPoineModel> localizer, int count = 80) => Enumerable.Range(1, count).Select(i => new VaccinationPoineModel()
        {
            Name = localizer["VaccinationPoineModel.Name", $"{i:d4}"],
            Phone = localizer["VaccinationPoineModel.Phone", $"{random.Next(1000, 2000)}"],
            Address = localizer["VaccinationPoineModel.Address", $"{i:d4}"],
            Principal = localizer["VaccinationPoineModel.Principal", $"{i:d4}"]
        }).ToList();
    }
}
