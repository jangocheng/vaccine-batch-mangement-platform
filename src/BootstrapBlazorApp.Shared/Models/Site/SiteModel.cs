﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Models.Sites
{
    public class SiteModel
    {
        [Display(Name = "KEY")]
        public Guid Id { get; set; }

        [Display(Name = "编码")]
        public string Code { get; set; }

        [Display(Name = "名称")]
        public string Name { get; set; }


        [Display(Name = "省")]
        public string Province { get; set; }



        [Display(Name = "市")]
        public string City { get; set; }



        [Display(Name = "区")]
        public string Area { get; set; }


        [Display(Name = "街道")]
        public string Street { get; set; }


        [Display(Name = "地址")]
        public string Address { get; set; }


        [Display(Name = "床架时间")]
        public DateTime CreationTime { get; set; }


        [Display(Name = "创建人员")]
        public Guid? CreationId { get; set; }


    }
}
