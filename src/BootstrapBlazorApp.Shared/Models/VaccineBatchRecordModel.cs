﻿using BootstrapBlazor.Components;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Models
{
    /// <summary>
    /// 疫苗批次记录
    /// </summary>
    public class VaccineBatchRecordModel:BaseModel
    {

        public BatchType Type { get; set; }

        public string BatchCode { get; set; }

        [AutoGenerateColumn(Ignore = true)]
        public Guid VaccineId { get; set; }

        public string VaccineName { get; set; }
        public DateTime ProductionDate { get; set; }

        public DateTime ValidDate { get; set; }
        public int Qty { get; set; }


        private static readonly Random random = new();
        public static List<VaccineBatchRecordModel> GenerateVaccineBatchRecordModel(IStringLocalizer<VaccineBatchRecordModel> localizer, int count = 80) => Enumerable.Range(1, count).Select(i => new VaccineBatchRecordModel()
        {
            Type = BatchType.In,
            BatchCode = localizer["VaccineDeliveryRecordModel.BatchCode", $"{random.Next(1000, 2000)}"],
            VaccineId = Guid.NewGuid(),
            ValidDate = System.DateTime.Now.AddDays(i - 1),
            ProductionDate = System.DateTime.Now.AddDays(i - 1),
            Qty = random.Next(1, 100),
        }).ToList();
    }
}
