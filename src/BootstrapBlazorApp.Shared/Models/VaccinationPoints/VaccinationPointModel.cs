﻿using System.ComponentModel.DataAnnotations;

namespace BootstrapBlazorApp.Shared.Models.VaccinationPoint
{
    public class VaccinationPointModel
    {
        public Guid Id { get; set; }

        [Display(Name = "站点名称")]
        public string Name { get; set; }

        [Display(Name = "站点地址")]
        public string Address { get; set; }

        [Display(Name = "联系手机")]
        public string Phone { get; set; }
        /// <summary>
        /// 负责人
        /// </summary>
        [Display(Name = "负责人")]
        public string Principal { get; set; }
    }
}
