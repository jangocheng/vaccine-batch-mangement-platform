﻿using BootstrapBlazor.Components;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Models
{
    /// <summary>
    ///  接种点基础信息
    /// </summary>
    public class VaccineDeliveryRecordModel : BaseModel
    {

        #region 采购

        [AutoGenerateColumn(Ignore = true)]
        public Guid PurchaseId { get; set; }

        public string PurchaseCompanyName { get; set; }

        public DateTime PurchaseDate { get; set; }

        public string VaccineName { get; set; }
        /// <summary>
        /// 发货数量
        /// </summary>
        public int Qty { get; set; }

        #endregion

        #region 批次

        [AutoGenerateColumn(Ignore = true)]
        public Guid VaccineBatchRecordId { get; set; }

        public string BatchCode { get; set; }

        public string ProductionDate { get; set; }

        public DateTime ValidDate { get; set; }

        public int VaccineBatchQty { get; set; }

        public DateTime DeliveryDate { get; set; }

        #endregion


        private static readonly Random random = new();
        public static List<VaccineDeliveryRecordModel> GenerateVaccineDeliveryRecordModel(IStringLocalizer<VaccineDeliveryRecordModel> localizer, int count = 80) => Enumerable.Range(1, count).Select(i => new VaccineDeliveryRecordModel()
        {
            BatchCode = localizer["VaccineDeliveryRecordModel.BatchCode", $"{random.Next(1000, 2000)}"],
            VaccineBatchRecordId = Guid.NewGuid(),
            DeliveryDate = System.DateTime.Now.AddDays(i - 1),
            Qty = random.Next(1, 100),
        }).ToList();
    }
}
