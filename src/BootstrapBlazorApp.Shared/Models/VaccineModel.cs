﻿﻿using BootstrapBlazor.Components;
using System;
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Models
{
    public class VaccineModel:BaseModel
    {

        public string Name { get; set; }
        public string Code { get; set; }

        public string Specifications { get; set; }


        public Guid CompanyId { get; set; }


        [AutoGenerateColumn(Ignore = true)]
        public string Company { get; set; }
    }

}
