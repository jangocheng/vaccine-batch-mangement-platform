﻿using BootstrapBlazor.Components;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Models
{
    public class ViewModelBase
    {

        /// <summary>
        /// Id
        /// </summary>
        [Display(Name = "主键")]
        [AutoGenerateColumn(Ignore = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        [Display(Name = "创建人名称")]
        [AutoGenerateColumn(Order = 70)]
        public string CreationName { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Display(Name = "更新时间")]
        [AutoGenerateColumn(Order = 70)]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 更新人名称
        /// </summary>
        [Display(Name = "更新人名称")]
        [AutoGenerateColumn(Order = 70)]
        public string UpdateName { get; set; }

        /// <summary>
        ///创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        [AutoGenerateColumn(Order = 70)]
        public DateTime CreationTime { get; set; }

        /// <summary>
        ///是否有效
        /// </summary>
        [Display(Name = "是否有效")]
        [AutoGenerateColumn(Order = 80)]
        public bool IsDelete { get; set; }
    }
}
