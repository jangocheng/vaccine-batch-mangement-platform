﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Models.POMain
{
    public class POMainModel
    {

        public virtual Guid? Id { get; set; }

        /// <summary>
        /// 需求单号
        /// </summary>
        public virtual string Code { get; set; }



        /// <summary>
        /// 生产厂家ID
        /// </summary>
        public virtual Guid? ManufactorId { get; set; }


        /// <summary>
        /// 需求日期
        /// </summary>
        public virtual DateTime DemandDate { get; set; }


        /// <summary>
        /// 接种站点ID
        /// </summary>
        public virtual Guid? VaccinationPointId { get; set; }

        /// <summary>
        /// 疫苗需求明细
        /// </summary>
        public ICollection<PODetailsModel> Items { get; set; }

    }
}
