﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Models.POMain
{
    public class PODetailsModel
    {
        /// <summary>
        /// KEY
        /// </summary>
        public virtual Guid? Id { get; set; }

        /// <summary>
        /// 需求主表ID
        /// </summary>
        public virtual Guid POMainId { get; set; }

        /// <summary>
        /// 疫苗ID
        /// </summary>
        public virtual Guid VaccineId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public virtual decimal Quantity { get; set; }

    }
}
