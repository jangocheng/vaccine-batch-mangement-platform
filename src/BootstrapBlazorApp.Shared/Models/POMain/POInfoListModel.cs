﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapBlazorApp.Shared.Models.POMain
{
    /// <summary>
    /// 
    /// </summary>
    public class POInfoListModel
    {

        /// <summary>
        /// 需求ID
        /// </summary>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// 需求主表ID
        /// </summary>
        public virtual Guid POMainId { get; set; }

        /// <summary>
        /// 疫苗ID
        /// </summary>
        [Display(Name = "疫苗")]
        public virtual Guid VaccineId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        [Display(Name = "数量")]
        public virtual decimal Quantity { get; set; }

        /// <summary>
        /// 累计收货数量
        /// </summary>
        [Display(Name = "累计收货数量")]
        public virtual decimal TotalReceiveQty { get; set; }

        /// <summary>
        /// 需求单号
        /// </summary>
        [Display(Name = "需求单号")]
        public virtual string Code { get; set; }

        /// <summary>
        /// 生产厂家ID
        /// </summary>
        public virtual Guid ManufactorId { get; set; }


        /// <summary>
        /// 需求日期
        /// </summary>
        [Display(Name = "需求日期")]
        public virtual DateTime DemandDate { get; set; }

        /// <summary>
        /// 疫苗名称
        /// </summary>
        [Display(Name = "疫苗名称")]
        public virtual string VaccineName { get; set; }
        /// <summary>
        /// 疫苗编号
        /// </summary>
        [Display(Name = "疫苗编号")]
        public virtual string VaccineCode { get; set; }


        /// <summary>
        /// 生产厂家
        /// </summary>
        [Display(Name = "生产厂家")]
        public virtual string CompanyName { get; set; }

        /// <summary>
        /// 接种站点ID
        /// </summary>
        public virtual Guid? VaccinationPointId { get; set; }

        /// <summary>
        /// 接种点名称
        /// </summary>
        [Display(Name = "接种站点")]
        public virtual string VaccinationPointName { get; set; }

    }
}
