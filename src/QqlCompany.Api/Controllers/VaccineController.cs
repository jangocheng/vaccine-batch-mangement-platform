﻿using QqlCompany.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using API.Entities;
using QqlCompany.Api.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace QqlCompany.Api.Controllers
{
    /// <summary>
    /// 厂家档案
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class VaccineController : ControllerBase
    {
        private readonly ILogger<VaccineController> _logger;
        private readonly PODbContext _context;

        public VaccineController(ILogger<VaccineController> logger, PODbContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public async Task<List<VaccineModel>> All(string? name)
        {
            _logger.LogInformation("param--> " + name);

            var query = from a in _context.Vaccines
                        join b in _context.Companies on a.CompanyId equals b.Id
                        select new VaccineModel() { Id = a.Id, Name = a.Name, Code = a.Code,
                            Company = b.CompanyName, CompanyId = a.CompanyId };
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(x => x.Name.Contains(name));
            }

            return await query.ToListAsync();
        }

        [HttpPost]
        [Route("save")]
        public async Task<bool> Save(VaccineModel model)
        {
            _logger.LogInformation("param--> " + JsonConvert.SerializeObject(model));
            //存储到db
            var entity = new Vaccine() { Id = Guid.NewGuid(), Name = model.Name, Code = DateTime.Now.ToString("yyMMddHHmmssfff"), CompanyId = model.CompanyId, Specifications = model.Specifications };
            _context.Add(entity);
            await _context.SaveChangesAsync();
            return true;
        }

        [HttpPut]
        [Route("update")]
        public async Task<bool> Update(VaccineModel model)
        {
            _logger.LogInformation("param--> " + JsonConvert.SerializeObject(model));
            //存储到db
            var vaccine = await _context.Vaccines.FindAsync(model.Id);
            if (vaccine == null) return false;
            vaccine.Name = model.Name;
            vaccine.Specifications = model.Specifications;
            vaccine.CompanyId = model.CompanyId;
            _context.Update(vaccine);
            await _context.SaveChangesAsync();
            return true;
        }


        [HttpDelete]
        [Route("remove/{id}")]
        public async Task<bool> Remove(Guid id)
        {
            _logger.LogInformation("param--> " + id);
            var vaccine = await _context.Vaccines.FindAsync(id);
            if (vaccine == null) return false;
            _context.Remove(vaccine);
            await _context.SaveChangesAsync();
            return true;
        }



        [HttpGet]
        [Route("companys")]
        public async Task<List<CompanyModel>> companys()
        {
            var companys = await _context.Companies.Select(x => new CompanyModel() { Id = x.Id, CompanyName = x.CompanyName }).ToListAsync();

            return companys;
        }



    }
}
