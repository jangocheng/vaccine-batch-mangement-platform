﻿using QqlCompany.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using API.Entities;
using QqlCompany.Api.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace QqlCompany.Api.Controllers
{
    /// <summary>
    /// 厂家档案
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CompanyController : ControllerBase
    {
        private readonly ILogger<CompanyController> _logger;
        private readonly PODbContext _context;

        public CompanyController(ILogger<CompanyController> logger, PODbContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public async Task<List<CompanyModel>> All(string? companyName)
        {
            _logger.LogInformation("param--> " + companyName);

            var query = _context.Companies.AsQueryable();
            if (!string.IsNullOrEmpty(companyName))
            {
                query = query.Where(x => x.CompanyName.Contains(companyName));
            }

            return await query.Select(x => new CompanyModel()
            {
                CompanyName = x.CompanyName,
                Id = x.Id,
                Phone = x.Phone,
                Address = x.Address
            }).ToListAsync();
        }

        [HttpPost]
        [Route("save")]
        public async Task<bool> SaveCompany(CompanyModel model)
        {
            _logger.LogInformation("param--> " + JsonConvert.SerializeObject(model));
            //存储到db
            var entity = new Company() { Id = Guid.NewGuid(), CompanyName = model.CompanyName, Address = model.Address };
            _context.Add(entity);
            await _context.SaveChangesAsync();
            return true;
        }

        [HttpPut]
        [Route("update")]
        public async Task<bool> UpdateCompany(CompanyModel model)
        {
            _logger.LogInformation("param--> " + JsonConvert.SerializeObject(model));
            //存储到db
            var company = await _context.Companies.FindAsync(model.Id);
            if (company == null) return false;
            company.CompanyName = model.CompanyName;
            company.Address = model.Address;
            company.Phone = model.Phone;
            _context.Update(company);
            await _context.SaveChangesAsync();
            return true;
        }


        [HttpDelete]
        [Route("remove/{id}")]
        public async Task<bool> RemoveCompany(string id)
        {
            _logger.LogInformation("param--> " + id);
            var company = await _context.Companies.FindAsync(id);
            if (company == null) return false;
            _context.Remove(company);
            await _context.SaveChangesAsync();
            return true;
        }


        /// <summary>
        /// 测试
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("testdapr")]
        public async Task<IActionResult> TestDaprClient()
        {
            _logger.LogInformation($" 测试");
            //var products = await _daprClient.InvokeMethodAsync<List<string>>(HttpMethod.Get, "siteapi", "/api/WeatherForecast");
            return Ok();
        }

    }
}
