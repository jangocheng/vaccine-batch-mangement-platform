﻿using QqlCompany.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using QqlCompany.Api.Infrastructure;
using API.Entities;
using Microsoft.EntityFrameworkCore;

namespace QqlCompany.Api.Controllers
{
    /// <summary>
    /// 疫苗批次档案
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class VaccineBatchRecordController : ControllerBase
    {
        private readonly ILogger<VaccineBatchRecordController> _logger;

        private readonly PODbContext _context;

        public VaccineBatchRecordController(ILogger<VaccineBatchRecordController> logger, PODbContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<VaccineBatchRecordDto>>> List(string? code)
        {
            _logger.LogInformation("param--> " + code);
            var query = (from a in _context.VaccineBatchRecords
                        join b in _context.Vaccines on a.VaccineId equals b.Id
                        select new VaccineBatchRecordDto() { BatchCode = a.BatchCode, Id = a.Id, VaccineId = b.Id,VaccineName=b.Name,
                            ValidDate = a.ValidDate, ProductionDate = a.ProductionDate, Qty = a.Qty, Type = a.Type }).AsQueryable();

            if (!string.IsNullOrEmpty(code))
            {
                query = query.Where(x => x.BatchCode.Contains(code));
            }
            
            return await query.ToListAsync();
        }

        [HttpPost]
        [Route("save")]
        public async Task<bool> Save(VaccineBatchRecordDto model)
        {
            _logger.LogInformation("param--> " + JsonConvert.SerializeObject(model));
            //存储到db
            var entity = new VaccineBatchRecordDto() { Id = Guid.NewGuid(), BatchCode = DateTime.Now.ToString("yyMMddHHmmssfff"), ProductionDate = DateTime.Now, VaccineId = model.VaccineId, ValidDate = model.ValidDate, Qty = model.Qty, Type = 1 };
            _context.Add(entity);
            await _context.SaveChangesAsync();
            return true;
        }

        [HttpPut]
        [Route("update")]
        public async Task<bool> Update(VaccineBatchRecordDto model)
        {
            _logger.LogInformation("param--> " + JsonConvert.SerializeObject(model));
            //存储到db
            var vaccineBatchRecord = await _context.VaccineBatchRecords.FindAsync(model.Id);
            if (vaccineBatchRecord == null) return false;
            vaccineBatchRecord.BatchCode = model.BatchCode;
            vaccineBatchRecord.Qty = model.Qty;
            vaccineBatchRecord.ValidDate = model.ValidDate;
            vaccineBatchRecord.ProductionDate = model.ProductionDate;
            vaccineBatchRecord.VaccineId = model.VaccineId;
            _context.Update(vaccineBatchRecord);
            await _context.SaveChangesAsync();
            return true;
        }


        [HttpDelete]
        [Route("remove/{id}")]
        public async Task<bool> Remove(Guid id)
        {
            _logger.LogInformation("param--> " + id);
            //存储到db
            var vaccineBatchRecord = await _context.VaccineBatchRecords.FindAsync(id);
            if (vaccineBatchRecord == null) return false;
            _context.Remove(vaccineBatchRecord);
            await _context.SaveChangesAsync();
            return true;
        }



        [HttpGet]
        [Route("vaccines")]
        public async Task<List<VaccineModel>> vaccines()
        {
            var vaccines = await _context.Vaccines.Select(x => new VaccineModel() { Code = x.Code, Name = x.Name, CompanyId = x.CompanyId, Id = x.Id }).ToListAsync();
            return vaccines;
        }

    }
}
