﻿using QqlCompany.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using QqlCompany.Api.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace QqlCompany.Api.Controllers
{
    /// <summary>
    /// 疫苗发货记录
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class VaccineDeliveryRecordController : ControllerBase
    {
        private readonly ILogger<VaccineDeliveryRecordController> _logger;
        private readonly PODbContext _context;

        public VaccineDeliveryRecordController(ILogger<VaccineDeliveryRecordController> logger, PODbContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public async Task<List<VaccineDeliveryRecordModel>> List(string? key)
        {
            _logger.LogInformation("lai l--> " + key);
            var vaccinationPoints = new List<VaccineDeliveryRecordModel>()
            {
                new VaccineDeliveryRecordModel(){Id=Guid.NewGuid(),BatchCode="",
                    PurchaseId=Guid.NewGuid(),VaccineBatchRecordId=Guid.NewGuid(),DeliveryDate=DateTime.Now,Qty=10
                },
                new VaccineDeliveryRecordModel(){ Id=Guid.NewGuid(),BatchCode="",PurchaseId=Guid.NewGuid(),VaccineBatchRecordId=Guid.NewGuid(),DeliveryDate=DateTime.Now,Qty=30}
            };

            var query = (from a in _context.VaccineDeliveryRecords
                         join b in _context.VaccineBatchRecords on a.VaccineBatchRecordId equals b.Id
                         join c in _context.PODemands on a.PurchaseId equals c.Id
                         join d in _context.PODetails on c.Id equals d.POMainId
                         join e in _context.Vaccines on b.VaccineId equals e.Id
                         join f in _context.Companies on e.CompanyId equals f.Id
                         join g in _context.VaccinationPoints on c.VaccinationPointId equals g.Id
                         select new VaccineDeliveryRecordModel()
                         {
                             Id = a.Id,
                             BatchCode = b.BatchCode,
                             Qty = a.Qty,
                             DeliveryDate = a.DeliveryDate,
                             ValidDate = b.ValidDate,
                             VaccineBatchQty = b.Qty,
                             VaccineBatchRecordId = b.Id,
                             VaccineName = e.Name,
                             PurchaseCompanyName = g.Name,
                             ProductionDate = b.ProductionDate,
                             PurchaseDate = b.ProductionDate,
                             PurchaseId = c.Id
                         }).AsQueryable();


            return await query.ToListAsync();
        }

        [HttpPost]
        [Route("save")]
        public IActionResult Save(VaccineDeliveryRecordModel model)
        {
            _logger.LogInformation("lai l--> " + JsonConvert.SerializeObject(model));
            //存储到db

            return Ok(true);
        }

        [HttpPut]
        [Route("update")]
        public IActionResult Update(VaccineDeliveryRecordModel model)
        {
            _logger.LogInformation("lai l--> " + JsonConvert.SerializeObject(model));
            //存储到db

            return Ok(true);
        }


        [HttpDelete]
        [Route("remove/{id}")]
        public IActionResult Remove(Guid id)
        {
            _logger.LogInformation("lai l--> " + id);
            //存储到db

            return Ok(true);
        }

    }
}
