﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using API.Entities;

namespace QqlCompany.Api.Infrastructure.EntityConfigurations
{
    public class VaccineBatchRecordConfiguration : IEntityTypeConfiguration<VaccineBatchRecord>
    {
        public void Configure(EntityTypeBuilder<VaccineBatchRecord> builder)
        {
            builder.ToTable("PC_VaccineBatchRecord");

            builder.HasKey(v => v.Id);


            builder.Property(v => v.BatchCode)
                .IsRequired();
        }
    }
}
