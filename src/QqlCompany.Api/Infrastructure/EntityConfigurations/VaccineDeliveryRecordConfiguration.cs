﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using API.Entities;

namespace QqlCompany.Api.Infrastructure.EntityConfigurations
{
    public class VaccineDeliveryRecordConfiguration : IEntityTypeConfiguration<VaccineDeliveryRecord>
    {
        public void Configure(EntityTypeBuilder<VaccineDeliveryRecord> builder)
        {
            builder.ToTable("PC_VaccineDeliveryRecord");

            builder.HasKey(v => v.Id);


            builder.Property(v => v.DeliveryDate)
                .IsRequired();
        }
    }
}
