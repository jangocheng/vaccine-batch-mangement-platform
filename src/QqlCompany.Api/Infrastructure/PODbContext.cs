﻿using API.Entities;
using Microsoft.EntityFrameworkCore;
using QqlCompany.Api.Infrastructure.EntityConfigurations;

namespace QqlCompany.Api.Infrastructure
{
    public class PODbContext : DbContext
    {
        public PODbContext(DbContextOptions<PODbContext> options)
            : base(options)
        {
        }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Vaccine> Vaccines { get; set; }
        public DbSet<VaccineBatchRecord> VaccineBatchRecords { get; set; }

        public DbSet<VaccineDeliveryRecord> VaccineDeliveryRecords { get; set; }
        public DbSet<PODemand> PODemands { get; set; }
        public DbSet<PODetails> PODetails { get; set; }

        public DbSet<Rdrecord> Rdrecords { get; set; }

        public DbSet<RdrecordDetails> RdrecordDetailss { get; set; }

        public DbSet<VaccinationPoint> VaccinationPoints { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new VaccineDeliveryRecordConfiguration());
            builder.ApplyConfiguration(new VaccineBatchRecordConfiguration());
        }
    }
}
