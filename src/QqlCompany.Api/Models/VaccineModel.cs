﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QqlCompany.Api.Models
{
    public class VaccineModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public string Specifications { get; set; }
        public Guid CompanyId { get; set; }
        public string Company { get; set; }
    }
}
