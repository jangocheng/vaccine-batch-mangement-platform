﻿namespace QqlCompany.Api.Models
{
    public class CompanyModel
    {
        public Guid Id { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
