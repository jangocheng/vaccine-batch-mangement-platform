﻿namespace QqlCompany.Api.Models
{
    /// <summary>
    /// 疫苗发货记录
    /// </summary>
    public class VaccineDeliveryRecordModel
    {
        public Guid Id { get; set; }

        #region 采购

        public Guid PurchaseId { get; set; }

        public string PurchaseCompanyName { get; set; }

        public DateTime PurchaseDate { get; set; }

        public string  VaccineName{get;set;}
        /// <summary>
        /// 发货数量
        /// </summary>
        public int Qty { get; set; }

        #endregion

        #region 批次

        public Guid VaccineBatchRecordId { get; set; }

        public string BatchCode { get; set; }

        public DateTime ProductionDate { get; set; }

        public DateTime ValidDate { get; set; }

        public int VaccineBatchQty { get; set; }

        public DateTime DeliveryDate { get; set; }

        #endregion

    }


    public class VaccineDeliveryRecordParam
    {
        public int Type { get; set; }

        public Guid PurchaseId { get; set; }

        public string PurchaseCompanyName { get; set; }


        public string ProductionDate { get; set; }

        public string VaccinationPointName { get; set; }

        public string BatchCode { get; set; }
    }
}
