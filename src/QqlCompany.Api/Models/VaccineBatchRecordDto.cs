﻿namespace QqlCompany.Api.Models
{
    public class VaccineBatchRecordDto
    {
        public Guid Id { get; set; }
        public int Type { get; set; }
        public string BatchCode { get; set; }
        public Guid VaccineId { get; set; }

        public string VaccineName { get; set; }
        public DateTime ProductionDate { get; set; }

        public DateTime ValidDate { get; set; }
        public int Qty { get; set; }
    }
}
