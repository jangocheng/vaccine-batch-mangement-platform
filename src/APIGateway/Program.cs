﻿var builder = WebApplication.CreateBuilder(args);
builder.Services.AddReverseProxy()
    .LoadFromConfig(builder.Configuration.GetSection("ReverseProxy"));
#region Id4
builder.Services.AddAuthentication("Bearer")
    .AddJwtBearer(options =>
    {
        options.Authority = "https://localhost:7010";
        options.RequireHttpsMetadata = false;
    });
builder.Services.AddAuthorization(options =>
    {
        options.AddPolicy("Id4Policy", policy =>
            policy.RequireAuthenticatedUser());
    });
#endregion
var app = builder.Build();
app.MapReverseProxy();
app.UseAuthentication();
app.UseAuthorization();
app.Run();
